<?php


namespace  WCS\Ms\Api\Helpers;


/**
 * Class Attributes
 * @package App\MoySklad\Api\Support
 */
class Attributes
{

    /**
     * @param $mValue
     * @param $sUuid
     * @param string $sNameFields
     * @return array
     */
    public static function getAttributeArrayByUuid($mValue, $sUuid, $sNameFields = '')
    {

        return [
            "meta" => [
                "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/$sUuid",
                "type" => "attributemetadata",
                "mediaType" => "application/json"
            ],
            'value' => $mValue
        ];

    }

    /**
     * @param $uuid
     * @return string
     */
    public static function getAttributeHref($uuid): string
    {
        return "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/$uuid";
    }

}
