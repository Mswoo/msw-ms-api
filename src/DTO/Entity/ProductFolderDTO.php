<?php

namespace WCS\Ms\Api\DTO\Entity;

/**
 *
 */
class ProductFolderDTO extends EntityDTO
{
    /**
     * @var string
     */
    protected string $entityType = 'productfolder';

}
