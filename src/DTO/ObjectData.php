<?php

namespace WCS\Ms\Api\DTO;

use Spatie\DataTransferObject\DataTransferObject;
use WCS\Ms\Api\DTO\Actions\FilterNullDTOValue;

class ObjectData extends DataTransferObject
{

    /**
     * @return array
     */
    public function toArray(): array
    {
        $FilterNullDTOValue = new FilterNullDTOValue();
        return array_filter(parent::toArray(), $FilterNullDTOValue);
    }


}
