<?php

namespace WCS\Ms\Api\DTO\Entity;

class UomDTO extends EntityDTO
{
    /**
     * @var string
     */
    protected string $entityType = 'uom';

}
