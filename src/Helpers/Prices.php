<?php


namespace  WCS\Ms\Api\Helpers;


/**
 * Class Prices
 * @package App\MoySklad\Api\Support
 */
class Prices
{

    /**
     * @param $iValue
     * @param $sUuid
     * @param string $sCurrency
     * @return array
     */
    public static function getSalePriceArrayByUuid($iValue, $sUuid, $sCurrency = 'RUB')
    {

        return [
            "value"     => (float)preg_replace('/\s/', '', $iValue) * 100,
            "currency"  => self::getMetaCurrency($sCurrency),
            'priceType' => self::getMetaPriceTypeByUuid($sUuid)
        ];

    }

    /**
     * @param $iValue
     * @param string $sCurrency
     * @return array
     */
    public static function getMinPriceArray($iValue, $sCurrency = 'RUB')
    {
        return [
            "value"     => (float)preg_replace('/\s/', '', $iValue) * 100,
            "currency"  => self::getMetaCurrency($sCurrency)
        ];

    }

    /**
     * @param string $sCurrency
     * @return \string[][]
     */
    public static function getMetaCurrency($sCurrency = 'RUB')
    {
        $sCurrencyUuid = ($sCurrency == 'USD') ? 'bc29d67a-a1b0-11ec-0a80-09b3001510dd' : '50f241a4-498f-11e9-9ff4-3150001692e7';

        return [
           "meta" => [
                    "href" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/$sCurrencyUuid",
                    "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/metadata",
                    "type" => "currency",
                    "mediaType" => "application/json"
                ]
        ];

    }


    /**
     * @param $sUuid
     * @return \string[][]
     */
    public static function getMetaPriceTypeByUuid($sUuid)
    {
        return [
          "meta" => [
                    "href" => "https://online.moysklad.ru/api/remap/1.2/context/companysettings/pricetype/$sUuid",
                    "type" => "pricetype",
                    "mediaType" => "application/json"
                ]

        ];       

    }

}

