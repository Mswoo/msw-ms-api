<?php


namespace WCS\Ms\Api\Helpers;


use WCS\Ms\Api\Interfaces\ObjectAsStringInterface;

/**
 * Class ParametersUrlApi
 * @package MSW\Ms\Api\Helpers
 */
class ParametersUrlApi implements ObjectAsStringInterface
{
    use ObjectAsString;
    /**
     * @var array
     */
    private array $urlParameters = [];

    /**
     * ParametersUrlApi constructor.
     * @param array $aParameters
     * @throws \Exception
     */
    public function __construct(array $aParameters)
    {
        if (!empty($aParameters)) {

            foreach ($aParameters as $sName => $sValue) {
                if (method_exists($this, $sName)) {
                    $this->{$sName}($sValue);
                } else {
                    $this->setParameterUrl([$sName => $sValue]);
                }
            }
        }

    }


    /**
     * @param $value
     */
    public function limit($value)
    {
        $this->setParameterUrl(['limit' => $value]);
    }

    /**
     * @param $value
     */
    public function offset($value)
    {
        $this->setParameterUrl(['offset' => $value]);
    }

    /**
     * @param $value
     */
    public function search($value)
    {
        $this->setParameterUrl(['search' => $value]);
    }

    /**
     * @param $value
     */
    public function expand($value)
    {
        $this->setParameterUrl(['expand' => $value]);
    }

    /**
     * @param array $parameters
     */
    public function setParameterUrl($parameters = [])
    {
        if (is_array($parameters) and !empty($parameters)) {
            foreach ($parameters as $key => $value) {
                $this->urlParameters[] = [$key, $value];
            }
        }
    }

    /**
     * @return string
     */
    public function getParameterUrl()
    {
        $query = '';

        if (!empty($this->urlParameters)) {
            foreach ($this->urlParameters as $key => $value) {
                $query .= $value[0] . '=' . urlencode($value[1]);
                $query .= (isset($this->urlParameters[$key + 1])) ? '&' : '';
            }
        }

        return !empty($query) ? '?' . $query : '';
    }

    /**
     * @return mixed|string
     */
    public function getString()
    {
        return $this->getParameterUrl();
    }
}