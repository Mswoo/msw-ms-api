<?php

namespace WCS\Ms\Api\DTO\Actions;


/**
 * Проверяет значение массива dto на null значения
 */
class FilterNullDTOValue
{
    /**
     * @param $value
     * @return mixed
     */
    public function __invoke($value)
    {
        return ($value !== null);
    }

}
