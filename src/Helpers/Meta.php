<?php


namespace  WCS\Ms\Api\Helpers;


class Meta
{
    /**
     * @param $sType
     * @param $sUuid
     * @return string[]
     */
    public static function get($sUuid, $sType = 'product')
    {
        return [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/$sType/$sUuid",
            "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/$sType/metadata",
            "type" => "$sType",
            "mediaType" => "application/json"
        ];

    }


    /**
     * @param $sUuid
     * @param $sType
     * @return string
     */
    public static function getHref($sUuid, $sType): string
    {
        return "https://online.moysklad.ru/api/remap/1.2/entity/$sType/$sUuid";
    }

    /**
     * @param $sType
     * @return string
     */
    public static function getMetadataHref($sType): string
    {
        return "https://online.moysklad.ru/api/remap/1.2/entity/$sType/metadata";
    }
}
