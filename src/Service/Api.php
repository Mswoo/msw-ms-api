<?php


namespace WCS\Ms\Api\Service;


use WCS\Ms\Api\Ms;

/**
 * Class Api
 * @package MSW\Ms\Api\Service
 */
class Api
{

    /**
     * @var
     */
    private static $oMs;

    /**
     * @var string
     */
    public static string $namespace = 'WCS\Ms\Api';

    /**
     * @param $entity
     * @param array $parameter
     * @return mixed
     * @throws \Exception
     */
    public static function entity($entity, $parameter = [])
    {
        return self::query('Entity', $entity, $parameter);
    }

    /**
     * @param $type
     * @param $class
     * @param array $parameter
     * @return mixed
     * @throws \Exception
     */
    public static function query($type, $class, $parameter = [])
    {

        $class = self::$namespace . "\\$type\\$class";

        if (is_object(self::$oMs) and class_exists($class)) {

            $oObject = new $class(self::$oMs);
            $oObject->aParameters = $parameter;

            return $oObject;

        }

        throw new \Exception('Вы не установили параметры');


    }

    /**
     * @param Ms $oMs
     */
    public static function setConfig(Ms $oMs)
    {
        self::$oMs = $oMs;
    }


}