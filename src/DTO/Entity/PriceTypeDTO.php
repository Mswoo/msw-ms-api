<?php

namespace WCS\Ms\Api\DTO\Entity;

/**
 *
 */
class PriceTypeDTO extends EntityDTO
{

    /**
     * @var bool
     */
    protected bool $metadataHref = false;

    /**
     * @var string
     */
    protected string $entityType = 'pricetype';

}
