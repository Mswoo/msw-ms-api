<?php

namespace WCS\Ms\Api\DTO\Entity;

/**
 *
 */
class ProductDTO extends EntityDTO
{

    protected string $entityType = 'product';


    /**
     * @var string|null
     */
    public ?string $name;

    /**
     * @var string|null
     */
    public ?string $article;

    /**
     * @var float|null
     */
    public ?float $weight;

    /**
     * @var string|null
     */
    public ?string $description;

    /**
     * @var  \WCS\Ms\Api\DTO\Entity\ProductFolderDTO|null
     */
    public  $productFolder;

    /**
     * @var \WCS\Ms\Api\DTO\Entity\ProductImageDTO[]|null
     */
    public  $images;

    /**
     * @var \WCS\Ms\Api\DTO\Entity\UomDTO|null
     */
    public  $uom;

    /**
     * @var \WCS\Ms\Api\DTO\Entity\PriceDTO|null
     */
    public  $minPrice;

    /**
     * @var \WCS\Ms\Api\DTO\Entity\SalePriceDTO[]|null
     */
    public  $salePrices;

    /**
     * @var \WCS\Ms\Api\DTO\Entity\ProductAttributeDTO[]|null
     */
    public  $attributes;




}
