<?php

namespace Service;


use WCS\Ms\Api\Ms;
use WCS\Ms\Api\Service\Api;
use PHPUnit\Framework\TestCase;

/**
 * Class ApiTest
 * @package Service
 */
class ApiTest extends TestCase
{


    /**
     * @throws \Exception
     */
    protected function setUp(): void
    {
        Api::setConfig(
            new Ms([
                'https://online.moysklad.ru/api/remap/1.2/',
                'admin@mswoo',
                '274014zaq!'
            ])
        );
    }


    /**
     * @throws \Exception
     */
    public function testInitApi()
    {
        $this->assertIsObject(Api::entity('Assortment'));
    }

    /**
     * @throws \Exception
     */
    public function testGetConnect()
    {
        $oAssortment = Api::entity('Assortment');
        $this->assertIsArray($oAssortment->get());

    }


    /**
     * @throws \Exception
     */
    public function testGetLimitUrl()
    {
        $oAssortment = Api::entity('Assortment', ['limit' => 1]);
        $oAssortment->get();

        $this->assertSame('https://online.moysklad.ru/api/remap/1.2/entity/assortment/?limit=1', $oAssortment->oQuery->getUrl());

    }

    /**
     * @throws \Exception
     */
    public function testGetApiEntityParameter()
    {
        $oAssortment = Api::entity('Assortment', ['limit' => 1]);

        $this->assertSame(['limit' => 1], $oAssortment->aParameters);

    }

    /**
     * @throws \Exception
     */
    public function testGetLimit()
    {
        $oAssortment = Api::entity('Assortment',['limit' => 1]);
        $oAssortment->get();

        $this->assertSame(1, count($oAssortment->getRows()));

    }





}
