<?php

namespace Entity;

use WCS\Ms\Api\Entity\Assortment;
use WCS\Ms\Api\Ms;
use PHPUnit\Framework\TestCase;

/**
 * Class AssortmentTest
 * @package Entity
 */
class AssortmentTest extends TestCase
{

    /**
     * @var Ms|object
     */
    protected object $aConfig;

    /**
     * @throws \Exception
     */
    protected function setUp(): void
    {
        $this->aConfig =
            new Ms([
                'https://online.moysklad.ru/api/remap/1.2/',
                'admin@mswoo',
                '274014zaq!'
            ]);
    }


    /**
     * @throws \Exception
     */
    public function testGetConnect()
    {
        $this->assertIsArray( Assortment::query($this->aConfig));
    }

    /**
     * @throws \Exception
     */
    public function testToString()
    {
        $oAssortment = new Assortment();
        $this->assertSame('assortment', (string)$oAssortment);
    }


}
