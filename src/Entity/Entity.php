<?php


namespace WCS\Ms\Api\Entity;

use WCS\Ms\Api\Core\Api;

/**
 * Class Entity
 * @package MSW\Ms\Api\Entity
 */
class Entity extends Api
{

    /**
     * @var string
     */
    protected string $type = 'entity';

}