<?php

namespace WCS\Ms\Api\DTO\Entity;

class SalePriceDTO extends PriceDTO
{
    /**
     * @var \WCS\Ms\Api\DTO\Entity\PriceTypeDTO
     */
    public  $priceType;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        if(isset($parameters['uuid'])){
            $parameters['priceType'] = ['uuid' => $parameters['uuid']];
            unset($parameters['uuid']);
        }

        parent::__construct($parameters);
    }


}
