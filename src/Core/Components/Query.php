<?php


namespace WCS\Ms\Api\Core\Components;

use Exception;
use WCS\Ms\Api\Helpers\GenerationApiUrl;
use WCS\Ms\Api\Helpers\ParametersUrlApi;
use WCS\Ms\Api\Ms;

/**
 * Class Query
 * @package MSW\Ms\Api\Core\Components
 */
class Query
{
    /**
     * @var bool
     */
    protected $http;

    /**
     * @var string
     */
    protected string $entityName;

    /**
     * @var string
     */
    protected string $type;

    /**
     * @var array
     */
    protected array $config;

    /**
     * @var array
     */
    protected array $aResponse;

    /**
     * @var string
     */
    protected array $urlParameters = [];
    /**
     * @var ParametersUrlApi
     */
    public ParametersUrlApi $aParameters;


    /**
     * @var array|mixed
     */
    protected  $headers;


    /**
     * Query constructor.
     * @param Ms $oMs
     * @param $object
     * @param array $aParameters
     * @param array $aHeaders
     * @throws Exception
     */
    public function __construct(Ms $oMs,  $object, $aParameters = [], $aHeaders = [])
    {
        if(!is_object($oMs)){
            throw new Exception('Config empty');
        }

        $this->config = $oMs->getConfig();

        $this->entityName = $object->getEntity();
        $this->type = $object->getType();
        $this->aParameters =  new ParametersUrlApi($aParameters);
        $this->headers = $aHeaders;

        $this->http = new Http([
            'auth' => [$this->config['login'], $this->config['password']],
            'http_errors' =>  false
        ]);
     }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get()
    {
        return $this->aResponse = $this->http->setHeaders($this->headers)->get($this->getUrl());
    }

    /**
     * @param array $aBody
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post($aBody = [])
    {
        return $this->aResponse = $this->http->setHeaders($this->headers)->setBody($aBody)->post($this->getUrl());
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->config['url'] . GenerationApiUrl::getListUrl($this->type, $this->entityName, $this->aParameters);
    }

    /**
     * @param $url
     */
    public function setUrl($url)
    {
        $this->entityName = $this->type . '/' . $this->entityName . '/' . $url;
    }


}