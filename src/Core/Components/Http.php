<?php


namespace WCS\Ms\Api\Core\Components;


use WCS\Ms\Api\Core\Request;

/**
 * Class Http
 * @package MSW\Ms\Api\Core\Components
 */
class Http
{

    /**
     * @var Request
     */
    private Request $request;

    /**
     * Http constructor.
     * @param array $aParameters
     */
    public function __construct($aParameters = [])
    {
        $this->request = new Request();

        $this->setParameters($aParameters);
    }


    /**
     * @param $aParameters
     */
    protected function setParameters($aParameters)
    {
        if (!empty($aParameters)) {
            foreach ($aParameters as $sName => $sValue) {
                $this->request->setParameter($sName, $sValue);
            }
        }

    }

    /**
     * @param $body
     * @return Http
     */
    public function setBody($body)
    {
        $this->request->setParameter('json', $body);
        return $this;
    }

    /**
     * @param $headers
     * @return Http
     */
    public function setHeaders($headers)
    {
        $this->request->setHeaders($headers);
        return $this;
    }




    /**
     * @param $sUrl
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($sUrl)
    {
        return $this->request->setUrl($sUrl)->send()->getContent();
    }

    /**
     *
     * @param $sUrl
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post($sUrl)
    {
        return $this->request->setMethod('POST')->setUrl($sUrl)->send()->getContent();
    }

    /**
     * @param $sUrl
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function put($sUrl)
    {
        return $this->request->setMethod('PUT')->setUrl($sUrl)->send()->getContent();
    }

}