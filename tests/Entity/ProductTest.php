<?php

namespace Entity;

use WCS\Ms\Api\Entity\Product;
use PHPUnit\Framework\TestCase;
use WCS\Ms\Api\Ms;

class ProductTest extends TestCase
{
    /**
     * @var Ms|object
     */
    protected object $aConfig;

    /**
     * @throws \Exception
     */
    protected function setUp(): void
    {
        $this->aConfig =
            new Ms([
                'https://online.moysklad.ru/api/remap/1.2/',
                'admin@mswoo',
                '274014zaq!'
            ]);
    }


    public function testSave()
    {
        $oProduct = new Product( $this->aConfig );
        $oProduct->setData([
            ['name' => '565'],
            ['name' => ' 5656']

        ]);

        try {
            $content = $oProduct->save();;
        } catch (\Exception $e){
            var_dump($e->getMessage());
        }

        $this->assertIsArray($content);

    }

    public function testUpdateOrCreateProducts()
    {

        try {
            $content = Product::updateOrCreateProducts( $this->aConfig, [
                ['name' => '565'],
                ['name' => ' 5656']

            ] );
        } catch (\Exception $e){
            var_dump($e->getMessage());
        }

        $this->assertIsArray($content);
        $this->assertSame(2, count($content));


    }

    public function testGet()
    {
        $oProduct = new Product( $this->aConfig );
        $oProduct->setParameters(['limit' => 1]);
        $oProduct->get();

        $this->assertSame(1, count($oProduct->getRows()));

    }

    public function testQuery()
    {
        $oProduct = new Product( $this->aConfig );
        $oProduct->setParameters(['limit' => 1]);
        $oProduct->query();

        $this->assertSame(1, count($oProduct->getRows()));

    }
}
