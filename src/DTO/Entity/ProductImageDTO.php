<?php

namespace WCS\Ms\Api\DTO\Entity;

use WCS\Ms\Api\DTO\ObjectData;

/**
 *
 */
class ProductImageDTO extends ObjectData
{
    /**
     * @var string|null
     */
    public string $filename;

    /**
     * @var string|null
     */
    public string $content;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        if(isset($parameters['content'])){
            $parameters['content'] = base64_encode( $parameters['content'] );
        }

        parent::__construct($parameters);
    }

}
