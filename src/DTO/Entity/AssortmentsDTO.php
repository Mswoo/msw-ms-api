<?php

namespace WCS\Ms\Api\DTO\Entity;

use WCS\Ms\Api\DTO\ObjectData;

/**
 *
 */
class AssortmentsDTO extends ObjectData
{
    /**
     * @var \WCS\Ms\Api\DTO\Entity\ProductDTO[]
     */
    public  $assortments;


    /**
     * @param $array
     * @return array
     */
    public static function getArray($array): array
    {
        $ObjectDto = new static($array);
        return $ObjectDto->toArray()['assortments'];
    }



}
