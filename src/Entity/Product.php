<?php


namespace WCS\Ms\Api\Entity;


use WCS\Ms\Api\DTO\Entity\AssortmentsDTO;
use WCS\Ms\Api\Ms;

/**
 * Class Product
 * @package MSW\Ms\Api\Entity
 */
class Product extends Entity
{
    /**
     * @var string
     */
    protected  $entity = 'product';


    /**
     * @param Ms $oMs
     * @param $aProducts
     * @param bool $bWebHookSend
     * @return mixed
     * @throws \Exception
     */
    public static function updateOrCreateProducts(Ms $oMs, $aProducts, bool $bWebHookSend = false)
    {
        $product = new static($oMs);

        if ($aProducts) {
            $product->setData(AssortmentsDTO::getArray(['assortments' => $aProducts]));
        }

        return $product->save($bWebHookSend);

    }

}