<?php


namespace WCS\Ms\Api\Helpers;


/**
 * Class GenerationApiUrl
 * @package MSW\Ms\Api\Helpers
 */
class GenerationApiUrl
{
    /**
     * @param string $type
     * @param $entityName
     * @param string $aParameters
     * @return string
     */
    public static function getListUrl($type = 'entity', $entityName, string $aParameters = '')
    {
        return "{$type}/{$entityName}/{$aParameters}";
    }

}