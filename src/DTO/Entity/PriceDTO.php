<?php

namespace WCS\Ms\Api\DTO\Entity;


/**
 *
 */
class PriceDTO extends EntityDTO
{

    /**
     * @var float
     */
    public float $value;

    /**
     * @var  \WCS\Ms\Api\DTO\Entity\CurrencyDTO|null
     */
    public $currency;


    /**
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        if(isset($parameters['value'])){
            $parameters['value'] = (float)preg_replace('/\s/', '', $parameters['value']) * 100;
        }

        parent::__construct($parameters);
    }
}
