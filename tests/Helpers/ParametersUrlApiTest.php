<?php

namespace Helpers;

use WCS\Ms\Api\Helpers\ParametersUrlApi;
use PHPUnit\Framework\TestCase;

/**
 * Class ParametersUrlApiTest
 * @package Helpers
 */
class ParametersUrlApiTest extends TestCase
{

    /**
     * @throws \Exception
     */
    public function testLimit()
    {
        $pParametersUrlApi = new ParametersUrlApi(['limit' => 1]);

        $this->assertSame('?limit=1', (string)$pParametersUrlApi);

    }

}
