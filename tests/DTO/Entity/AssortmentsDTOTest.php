<?php

namespace DTO\Entity;

use WCS\Ms\Api\DTO\Entity\AssortmentsDTO;
use PHPUnit\Framework\TestCase;

class AssortmentsDTOTest extends TestCase
{

    public function testGet()
    {

        $dto = AssortmentsDTO::getArray([
            'assortments' => [
                [
                    'uuid' => '222',
                    'name' => 'TEst',
                    'article' => '12345',
                    'weight' => 3.33,
                    'description' => 'test desc',
                    'productFolder' => ['uuid' => 'uuid'],
                    'images' => [
                        ['filename' => 'uuid.gg', 'content' => 'ffkjk']
                    ],
                    'uom' => ['uuid' => 'uuid'],
                    'minPrice' => ['value' => 44],
                    'salePrices' => [
                        ['uuid' => 'uuid', 'value' => 45545, 'currency' => ['uuid' => 'uuid']],
                        ['uuid' => 'uuid2', 'value' => 67788, 'currency' => ['uuid' => 'uuid']]
                    ],
                    'attributes' => [
                        ['uuid' => 'uuid', 'value' => 0.0],
                        ['uuid' => 'uuid2', 'value' => 67788]
                    ],
                ],
                [
                    'uuid' => '333',
                    'name' => 'TEst2',
                    'article' => '22222'
                ],
                [
                    'uuid' => '444',
                    'name' => 'TEst2',
                    'article' => '444'
                ]
            ]
        ]);

        print_r($dto);


        $this->assertIsArray($dto);
        $this->assertCount(3, $dto);
    }

}
