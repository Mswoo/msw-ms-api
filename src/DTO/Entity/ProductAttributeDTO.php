<?php

namespace WCS\Ms\Api\DTO\Entity;

use WCS\Ms\Api\Helpers\Attributes;

class ProductAttributeDTO extends EntityDTO
{

    /**
     * @var string
     */
    protected string $entityType = 'attributemetadata';

    /**
     * Mixed types:
     *
     * @var mixed|null
     */
    public $value;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        if(isset($parameters['uuid']) and isset($this->entityType)){
            $parameters['meta'] = [
                'href' => Attributes::getAttributeHref($parameters['uuid']),
                'type' => $this->entityType
            ];

            unset($parameters['uuid']);
        }

        parent::__construct($parameters);
    }

}
