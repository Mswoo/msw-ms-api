<?php

namespace WCS\Ms\Api\DTO;

class MetaDTO extends ObjectData
{
    /**
     * @var string
     */
    public string $href;
    /**
     * @var string|null
     */
    public ?string $metadataHref;
    /**
     * @var string
     */
    public string $type;
    /**
     * @var string
     */
    public string $mediaType = "application/json";



}
