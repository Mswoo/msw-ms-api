<?php


namespace WCS\Ms\Api\Core;


use WCS\Ms\Api\Core\Components\Query;
use WCS\Ms\Api\Helpers\ObjectAsString;
use WCS\Ms\Api\Interfaces\ObjectAsStringInterface;
/**
 * Class Api
 * @package WCS\Ms\Api\Core
 */
class Api implements ObjectAsStringInterface
{
    use ObjectAsString;

    /**
     * @var string
     */
    protected string $type;

    /**
     * @var
     */
    protected  $entity;


    /**
     * @var string
     */
    public string $entityName;
    /**
     * @var object
     */
    public object $oMs;
    /**
     * @var array
     */
    public array $aParameters = [];

    /**
     * @var array
     */
    public array $headers = [];



    /**
     * @var mixed
     */
    protected $aResponse;
    /**
     * @var Query
     */
    public Query $oQuery;

    /**
     * @var array
     */
    protected $data = [];



    /**
     * Api constructor.
     * @param object $oMs
     */
    public function __construct(object $oMs)
    {
        $this->oMs = $oMs;
    }


    /**
     * @param string $method
     * @param bool $data
     * @return mixed
     * @throws \Exception
     */
    public  function query($method = 'get', $data = false)
    {
        $this->oQuery = new Query( $this->oMs, $this,  $this->aParameters,  $this->headers);
        return $this->aResponse = $this->oQuery->$method($data);
    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function get()
    {
        return $this->query();
    }


    /**
     * @param false $bWebHookSend
     * @param array $aHeaders
     * @return mixed
     * @throws \Exception
     */
    public function save($bWebHookSend = false, $aHeaders = [])
    {
        if (!$bWebHookSend) {
            $this->headers['X-Lognex-WebHook-Disable'] = true;
        }

        if ($this->data) {
            $aData = $this->data;
            $this->data = null;

            return $this->query('post', $aData);
        }

        throw new Exception('Нет данных для сохранения');;
    }

    /**
     * @param array $aParameters
     */
    public function setParameters(array $aParameters)
    {
        $this->aParameters = $aParameters;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers): void
    {
        if($headers){
            $this->headers = $headers;
        }

    }

    /**
     * @param array $data
     * @return Api
     */
    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getRows()
    {
        return $this->aResponse['rows'];
    }


    /**
     * @return mixed|string
     */
    public function getString()
    {
        return $this->entityName;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

}