<?php


namespace WCS\Ms\Api;


/**
 * Class Ms
 * @package MSW\Ms\Api
 */
class Ms
{

    /**
     * @var
     */
    private array $aConfig;

    /**
     * Ms constructor.
     * @param $aConfig
     * @throws \Exception
     */
    public function __construct($aConfig)
    {
        if (!is_array($aConfig) and count($aConfig) < 3) {
            throw new \Exception('Не указали все конфиги');
        }

        $this->aConfig = [
            'url' => $aConfig[0],
            'login' => $aConfig[1],
            'password' => $aConfig[2]
        ];

    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->aConfig;
    }

}