<?php

namespace DTO\Actions;

use WCS\Ms\Api\DTO\Actions\FilterNullDTOValue;
use PHPUnit\Framework\TestCase;
use WCS\Ms\Api\Service\Api;

/**
 * Class FilterNullDTOValueTest
 * @package DTO\Actions
 */
class FilterNullDTOValueTest extends TestCase
{


    /**
     *
     */
    public function test__invokeIsObjsect()
    {
        $FilterNullDTOValue = new FilterNullDTOValue();
        $this->assertIsObject($FilterNullDTOValue);
    }


    /**
     *
     */
    public function test__invokeIsFalse()
    {
        $FilterNullDTOValue = new FilterNullDTOValue();
        $this->assertSame(false, $FilterNullDTOValue(null));

    }

    /**
     *
     */
    public function test__invokeIsTrue()
    {
        $FilterNullDTOValue = new FilterNullDTOValue();
        $this->assertSame(true, $FilterNullDTOValue(0.0));

    }
}
