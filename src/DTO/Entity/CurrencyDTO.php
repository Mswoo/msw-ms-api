<?php

namespace WCS\Ms\Api\DTO\Entity;

class CurrencyDTO extends EntityDTO
{

    /**
     * @var string
     */
    protected string $entityType = 'currency';
}
