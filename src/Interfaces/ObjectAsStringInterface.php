<?php


namespace WCS\Ms\Api\Interfaces;


/**
 * Interface ObjectAsStringInterface
 * @package MSW\Ms\Api\Interfaces
 */
interface ObjectAsStringInterface
{
    /**
     * @return mixed
     */
    public function getString();

    /**
     * @return mixed
     */
    public function __toString();
}