<?php

namespace WCS\Ms\Api\DTO\Entity;


use WCS\Ms\Api\DTO\ObjectData;
use WCS\Ms\Api\Helpers\Meta;

/**
 *
 */
class EntityDTO extends ObjectData
{
    /** @var \WCS\Ms\Api\DTO\MetaDTO|null */

    public $meta;

    /**
     * @var string|null
     */
    protected string $entityType;

    /**
     * @var bool
     */
    protected bool $metadataHref = true;


    /**
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {

        if (isset($parameters['uuid']) and isset($this->entityType)) {
            $parameters = array_merge([
                'meta' => [
                    'href' => Meta::getHref($parameters['uuid'], $this->entityType),
                    'metadataHref' => ($this->metadataHref) ? Meta::getMetadataHref($this->entityType) : null,
                    'type' => $this->entityType
                ]
            ], $parameters);
            unset($parameters['uuid']);
        }

        parent::__construct($parameters);
    }


    /**
     * @param $array
     * @return array
     */
    public static function fromArray($array): array
    {
        $fields = [];

        foreach ($array as $fieldName => $fieldValue) {
            if (is_string($fieldName) and !empty($fieldValue)) {
                $fields[$fieldName] = $fieldValue;
            }
        }

        $ObjectDto = new static($fields);

        return $ObjectDto->toArray();
    }

}
