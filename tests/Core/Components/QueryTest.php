<?php

namespace Core\Components;

use WCS\Ms\Api\Core\Components\Query;
use WCS\Ms\Api\Entity\Assortment;
use WCS\Ms\Api\Ms;
use PHPUnit\Framework\TestCase;

/**
 * Class QueryTest
 * @package Core\Components
 */
class QueryTest extends TestCase
{

    protected object $aConfig;

    /**
     * @throws \Exception
     */
    protected function setUp(): void
    {
        $this->aConfig =
            new Ms([
                'https://online.moysklad.ru/api/remap/1.2/',
                'admin@mswoo',
                '274014zaq!'
            ]);
    }

    /**
     * @throws \Exception
     */
    public function testGetLimitUrl()
    {
        $oQuery = new Query($this->aConfig, new Assortment($this->aConfig), ['limit' => 1]);

        $this->assertSame('https://online.moysklad.ru/api/remap/1.2/entity/assortment/?limit=1', $oQuery->getUrl());

    }

}
