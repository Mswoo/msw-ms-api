<?php


namespace WCS\Ms\Api\Core;


use WCS\Ms\Api\Helpers\ObjectAsString;
use WCS\Ms\Api\Interfaces\ObjectAsStringInterface;

/**
 * Class Error
 * @package MSW\Ms\Api\Core
 */
class Error implements ObjectAsStringInterface
{
    use ObjectAsString;

    /**
     * @var array
     */
    private $error;

    /**
     * Error constructor.
     * @param $error
     */
    public function __construct($error)
    {
        $this->error = $error;
        return $this->getErrorMessage();
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->error['errors'][0]['error'];
    }

    /**
     * @return mixed|string
     */
    public function getString()
    {
        $string = '';

        if($this->error and isset($this->error['errors']) and is_array($this->error['errors'])){

            foreach ($this->error['errors'] as $error){
                $string .=  (isset($error['code'])) ? 'Code:' . $error['code'] . PHP_EOL : PHP_EOL;
                $string .=  (isset($error['parameter'])) ? 'Parameter:' . $error['parameter'] . PHP_EOL : PHP_EOL;
                $string .=  (isset($error['error'])) ? 'Error:' . $error['error'] . PHP_EOL : PHP_EOL;
                $string .=  (isset($error['error_message'])) ? 'Error_message:' . $error['error_message'] . PHP_EOL : PHP_EOL;

            }

        }


        return $string;

    }

}