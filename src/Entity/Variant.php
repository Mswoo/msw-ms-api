<?php


namespace WCS\Ms\Api\Entity;


/**
 * Class Variant
 * @package MSW\Ms\Api\Entity
 */
class Variant extends Entity
{
    /**
     * @var string
     */
    protected  $entity = 'variant';

}