<?php


namespace  WCS\Ms\Api\Helpers;


/**
 * Trait ObjectAsString
 * @package MSW\Ms\Api\Helpers
 */
trait ObjectAsString
{

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getString();
    }


}