<?php


namespace WCS\Ms\Api\Core;


use Exception;
use GuzzleHttp\Client;
use WCS\Ms\Api\Helpers\Json;

/**
 * Class Request
 * @package WCS\Ms\Api\Core
 */
class Request
{
    /**
     * @var Client
     */
    private Client $client;

    /**
     * @var object
     */
    private object $response;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var array
     */
    private array $parameter = [];

    /**
     * @var string
     */
    private string $method = 'GET';


    /**
     * Request constructor.
     * @param string $sUrl
     */
    public function __construct($sUrl = '')
    {
        $this->client = new Client();

        if(!empty($sUrl)){
            $this->setUrl($sUrl);
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getContent()
    {
        $content = $this->response->getBody()->getContents();

        if($this->response->getStatusCode() == '200'){
            return Json::decode($content);
        }

        $oError = new Error(Json::decode($content));
        throw new Exception((string)$oError);

    }


    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setParameter($key, $value)
    {
        $this->parameter[$key] = $value;
        return $this;
    }

    /**
     * @param $headers
     * @return $this
     */
    public function setHeaders($headers)
    {
        if(!$headers){
            return $this;
        }

        foreach ($headers as $key => $value){
            $this->parameter['headers'][$key] = $value;
        }

        return $this;
    }


    /**
     * @param $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send()
    {
        $this->response = $this->client->request($this->method, $this->url, $this->parameter);

        return $this;
    }

}