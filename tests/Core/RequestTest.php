<?php

namespace Core;

use WCS\Ms\Api\Core\Request;
use PHPUnit\Framework\TestCase;

/**
 * Class RequestTest
 * @package Core
 */
class RequestTest extends TestCase
{
    /**
     *
     */
    public function testInitApi()
    {
        $oRequest = new Request();
        $this->assertIsObject($oRequest);

    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testRequestConnect()
    {
        $oRequest = new Request('https://online.moysklad.ru/api/remap/1.2/entity/assortment/');
        $oRequest->setParameter('auth', ['admin@mswoo', '274014zaq!']);
        $oRequest->setParameter('http_errors', false);
        $content = $oRequest->send()->getContent();
        $this->assertIsArray($content);

    }


    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testPost()
    {
        $oRequest = new Request('https://online.moysklad.ru/api/remap/1.2/entity/product/');
        $oRequest->setParameter('auth', ['admin@mswoo', '274014zaq!']);
        $oRequest->setParameter('http_errors', false);
        $oRequest->setMethod('POST');

        try {
            $content = $oRequest->setParameter('json', ['name' => 'test'])->send()->getContent();
        } catch (\Exception $e){
            var_dump($e->getMessage());
        }

        $this->assertIsArray($content);

    }

}
