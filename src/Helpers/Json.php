<?php


namespace WCS\Ms\Api\Helpers;


/**
 * Class Json
 * @package MSW\Ms\Api\Helpers
 */
class Json
{

    /**
     * @param $sContent
     * @return false|string
     */
    public static function encode($sContent )
    {
        return json_encode($sContent);
    }

    /**
     * @param $oContent
     * @return mixed
     */
    public static function decode($oContent )
    {
        return json_decode( $oContent, true );
    }


}